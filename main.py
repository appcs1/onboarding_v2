import sys
from handlers.utils.Excelsheet import Excelsheet

def main():
    excelsheet = Excelsheet()
    excelsheet_path = excelsheet.path
    app_name = excelsheet.app_name
    sheetnames = excelsheet.sheetnames
    onboarding_environment = excelsheet.onboarding_environment
    # print(type(excelsheet_path), type(app_name), type(sheetnames), type(onboarding_environment))

    intro_header = f'''
=============================================================
     ___        _                         _ _                
    / _ \ _ __ | |__   ___   __ _ _ __ __| (_)_ __   __ _ 
   | | | | '_ \| '_ \ / _ \ / _` | '__/ _` | | '_ \ / _` |
   | |_| | | | | |_) | (_) | (_| | | | (_| | | | | | (_| |
    \___/|_| |_|_.__/ \___/ \__,_|_|  \__,_|_|_| |_|\__, |
                                                    |___/ 
                                                          v2
============================================================='''
    print(intro_header)

    if excelsheet_path == None:
        intro_content = '''
🚫 Unable to find excelsheet!

=============================================================
'''
        print(intro_content)
        sys.exit()
    if sheetnames == None:
        intro_content = '''
🚫 No services detected!

=============================================================
'''
        print(intro_content)
        sys.exit()
    if excelsheet_path != None and sheetnames != None:
        intro_content = f'''
Excelsheet Path: {excelsheet_path}
App name: {app_name}
Sheetnames: {sheetnames}
Onboarding environment: {onboarding_environment}

Do ensure that the above information is accurate before
generating files.

=============================================================
'''
        print(intro_content)
        i = True
        valid_input = ['y', 'n', 'yes', 'no']
        while i:
            user_input = input('Generate files? [Y/N]: ')
            if user_input in valid_input:
                if user_input == 'n' or user_input == 'no':
                    print('Exiting programme, bye!\n')
                    sys.exit()
                if user_input == 'y' or user_input == 'yes':
                    i = False
                    print('hello')
                    # if 'mcns' in sheetnames:
                    #     mcns_handler()
                    # if 'mds' in sheetnames:
                    #     mds_handler()
                    # if 'mpds' in sheetnames:
                    #     mpds_handler()
                    # if 'sft' in sheetnames:
                    #     sft_handler()
                    # if 'sfs' in sheetnames:
                    #     sfs_handler()
                    # if 'myinfo' in sheetnames:
                    #     myinfo_handler()
                    # if 'survey' in sheetnames:
                    #     survey_handler()

main()