import os
import pandas as pd
from openpyxl import load_workbook

class Excelsheet:
    def __init__(self):
        self.path = self.get_excelsheet_path()
        self.sheetnames = self.get_sheetnames()
        self.app_name = self.get_app_name()
        self.onboarding_environment = self.get_onboarding_environment()

    def get_excelsheet_path(self):
        # If excel found, returns string of excelsheet path. If excel not found, return None
        folder_path = './excels/'
        for file in os.listdir(folder_path):
            if '.xlsx' in file:
                excelsheet_path = folder_path + file
                return excelsheet_path

    def get_sheetnames(self):
        try:
            excelsheet = load_workbook(self.path)
            sheetnames_full_list = excelsheet.sheetnames
            sheetnames_list = []
            for sheetname in sheetnames_full_list:
                sheetname = sheetname.strip().lower()
                if sheetname not in ['guide', 'faq', 'changelog', 'project details', 'sso', 'uds', 'eai', 'sftp', 'corppass', 'ca']:
                    sheetnames_list.append(sheetname.upper())
            return sheetnames_list
        except:
            return None
        
    def get_app_name(self):
        try:
            app_name = self.path.split('/')[-1].replace('.xlsx', '')
            return app_name
        except:
            return None
    
    def get_onboarding_environment(self):
        try:
            pmt_environment = self.app_name.split('-')[-1]
            if pmt_environment in ['dev', 'qa', 'sit']:
                environment = 'SIT'
            else:
                environment = 'Prod'
            return environment
        except:
            return None

    def get_service_sheet(self, service):
        try:
            service_sheet = pd.read_excel(self.get_excelsheet_path(), service)
            return service_sheet
        except:
            return None